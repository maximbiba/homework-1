package Conditions;

public class Conditions {
    public static int sumOrAdd(int a, int b)   // Если а – четное посчитать а*б, иначе а+б
    {
        int response;
        if (a % 2 == 0)
        {
            response = a * b;
        } else
        {
            response = a + b;
        }
        return response;
    }


    public static int findQuarter(int a, int b)   // Определить какой четверти принадлежит точка с координатами (х,у)
    {
        int response;
        if (a > 0 && b > 0)
        {
            response = 1;
        }
        else if (a < 0 && b > 0)
        {
            response = 2;
        }
        else if (a < 0 && b < 0)
        {
            response = 3;
        }
        else response = 4;
        return response;
    }


    public static int sumOfPositiveElements(int a, int b, int c)   //  Найти суммы только положительных из трех чисел
    {
        int response = 0;
        if (a > 0)
        {
            response += a;
        }
        if (b > 0)
        {
            response += b;
        }
        if (c > 0)
        {
            response += c;
        }
        return response;
    }



    public static int calculateMax(int a, int b, int c)   //  Посчитать выражение макс(а*б*с, а+б+с)+3
    {
        int response;
        int x = a * b * c;
        int y = a + b + c;
        if (x > y)
        {
            response = x;
        }
        else response = y;
        return response + 3;
    }



    public static char findStudentMark(int a)  // Оценить рейтинг студентов
    {
        char response = 0;
        if (a >= 0 && a <= 19)
        {
            response = 'F';
        }
        else if (a >= 20 && a <= 39)
        {
            response = 'E';
        }
        else if (a >= 40 && a <= 59)
        {
            response = 'D';
        }
        else if (a >= 60 && a <= 74)
        {
            response = 'C';
        }
        else if (a >= 75 && a <= 89)
        {
            response = 'B';
        }
        else if (a >= 90 && a <= 100)
        {
            response = 'A';
        }
        return response;
    }

}
