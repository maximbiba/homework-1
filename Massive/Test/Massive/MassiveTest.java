package Massive;

import org.junit.Test;
import static org.junit.Assert.*;
import static junit.framework.TestCase.assertEquals;

public class MassiveTest {

    @Test
    public void minElement() {
            int result = Massive.minElement(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
            assertEquals(result, 0);
        }
    @Test
    public void minElementTest2()
    {
        int result = Massive.minElement(new int[] {10, 9, 8, 7, 6, 5, 4, 3, 2, 1});
        assertEquals(result, 1);
    }
    @Test
    public void minElementTest3()
    {
        int result = Massive.minElement(new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 10});
        assertEquals(result, 0);
    }



    @Test
    public void maxElementTest1()
    {
        int result = Massive.maxElement(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        assertEquals(result, 10);
    }
    @Test
    public void maxElementTest2()
    {
        int result = Massive.maxElement(new int[] {10, 9, 8, 7, 6, 5, 4, 3, 2, 1});
        assertEquals(result, 10);
    }
    @Test
    public void maxElementTest3()
    {
        int result = Massive.maxElement(new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 10});
        assertEquals(result, 10);
    }

    @Test
    public void indexMinElementTest1()
    {
        int result = Massive.indexMinElement(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        assertEquals(result, 0);
    }
    @Test
    public void indexMinElementTest2()
    {
        int result = Massive.indexMinElement(new int[] {10, 9, 8, 7, 6, 5, 4, 3, 2, 1});
        assertEquals(result, 9);
    }
    @Test
    public void indexMinElementTest3()
    {
        int result = Massive.indexMinElement(new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 10});
        assertEquals(result, 0);
    }

    @Test
    public void indexMaxElementTest1()
    {
        int result = Massive.indexMaxElement(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        assertEquals(result, 9);
    }
    @Test
    public void indexMaxElementTest2()
    {
        int result = Massive.indexMaxElement(new int[] {10, 9, 8, 7, 6, 5, 4, 3, 2, 1});
        assertEquals(result, 0);
    }
    @Test
    public void indexMaxElementTest3()
    {
        int result = Massive.indexMaxElement(new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 10});
        assertEquals(result, 9);
    }


    @Test
    public void sumElementsOddIndexTest()
    {
        int result = Massive.sumElementsOddIndex(new int[]{5, 10, 15, 20, 25, 30, 35, 40, 45, 50});
        assertEquals(result, 120);
    }


    @Test
    public void reversArrayTest()
    {
        int[] result = Massive.reversArray(new int[]{1, 2, 3, 4, 5});
        assertArrayEquals(result, new int[]{5, 4, 3, 2, 1});
    }


    @Test
    public void countOddElementsTest()
    {
        int result = Massive.countOddElements(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        assertEquals(result, 5);
    }


    @Test
    public void replaceHalfArrayTest()
    {
        int[] result = Massive.replaceHalfArray(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        assertArrayEquals(result, new int[]{6, 7, 8, 9, 10, 1, 2, 3, 4, 5});
    }


    @Test
    public void BubbleSortTest()
    {
        int[] result = Massive.BubbleSort(new int[]{0, 0, 2, 10, 5, 4, 8, 1, 0, 7, 9, 0, 11, 12, 0, 14, 15});
        assertArrayEquals(result, new int[]{0, 0, 0, 0, 0, 1, 2, 4, 5, 7, 8, 9, 10, 11, 12, 14, 15});
    }
    @Test
    public void selectionSortTest()
    {
        int[] result = Massive.selectionSort(new int[]{0, 0, 2, 10, 5, 4, 8, 1, 0, 7, 9, 0, 11, 12, 0, 14, 15});
        assertArrayEquals(result, new int[]{0, 0, 0, 0, 0, 1, 2, 4, 5, 7, 8, 9, 10, 11, 12, 14, 15});
    }
    @Test
    public void insertionSortTest()
    {
        {
            int[] result = Massive.insertionSort(new int[]{0, 0, 2, 10, 5, 4, 8, 1, 0, 7, 9, 0, 11, 12, 0, 14, 15});
            assertArrayEquals(result, new int[]{0, 0, 0, 0, 0, 1, 2, 4, 5, 7, 8, 9, 10, 11, 12, 14, 15});
        }
    }


}
