package Function;

public class Function {
    public static String getDayOfWeek(int a)   //  Получить строковое название дня недели по номеру дня.
    {
        String response;
        switch (a)
        {
            case 1:
                response = "Понедельник";
                break;
            case 2:
                response = "Вторник";
                break;
            case 3:
                response = "Среда";
                break;
            case 4:
                response = "Четверг";
                break;
            case 5:
                response = "Пятница";
                break;
            case 6:
                response = "Суббота";
                break;
            case 7:
                response = "Воскресенье";
                break;
            default: throw new IndexOutOfBoundsException();
        }
        return response;
    }




    public static float calculateDistance(int x1, int y1, int x2, int y2)    //  Найти расстояние между двумя точками в двумерном декартовом пространстве.
    {
        return (float) Math.sqrt( ( Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2) ) );
    }
}
