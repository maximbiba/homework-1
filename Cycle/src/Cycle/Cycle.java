package Cycle;

public class Cycle {
    public static int[] sumAndCountEvenElements()   // Найти сумму четных чисел и их количество в диапазоне от 1 до 99
    {
        int sum = 0, count = 0;
        for (int i = 1; i < 99; i++) {
            if (i % 2 == 0)
            {
                sum += i;
                count++;
            }
        }
        return new int[] {sum, count};
    }




    public static boolean isSimpleNumber(int in)  //  Проверить простое ли число?
    {
        boolean response = true;
        for (int i = 2; i < in; i++) {
            if (in % i  == 0)
            {
                response = false;
            }
        }
        return response;
    }




    public static int mySqrtChecking(int a)  // Найти корень натурального числа с точностью до целого( последовательний подбор)
    {
        int i = 1;
        while (i * i <= a)
        {
            i++;
        }
        return i-1;
    }

    public static int mySqrtBinSearch(int a) {  // Найти корень натурального числа с точностью до целого(бинарний поиск)
        int response = a;

        while (response * response > a) {
            response /= 2;
        }
        while (response * response < a) {
            response++;
        }
        return response;
    }




    public static int myFactorial(int a)  //  Вычислить факториал числа n. n! = 1*2*…*n-1*n;!
    {
        int response = 1;
        if (a > 1)
        {
            for (int i = 2; i <= a; i++) {
                response *= i;
            }
        }
        return response;
    }



    public static int sumOfNumbers(int a)  //  Посчитать сумму цифр заданного числа
    {
        int result = 0;
        while (a != 0)
        {
            result += a % 10;
            a /= 10;
        }
        return result;
    }




    public static int reversNumbers(int a)   // Вывести число которое является зеркальным отображением заданого числа
    {
        int result = 0;
        while (a > 0)
        {
            result = (result + (a % 10)) * 10;
            a = a / 10;
        }
        return result / 10;
    }









}
